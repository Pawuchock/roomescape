﻿using UnityEngine;
using System.Collections;

public static class ColisionDetector {

	public static GameObject detectCollision ()
	{
		Pick pick = new Pick ();
		int x = Screen.width / 2;
		int y = Screen.height / 2;
		Ray ray = Camera.main.ScreenPointToRay (new Vector3 (x, y));
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) { 
			if (hit.distance < 2f) {
				//					 if something hit at range of 2f, send the message
									Debug.Log ("Name " + hit.collider.gameObject.name + " Distance " + hit.distance / 100);
				pick.currObject = hit.collider.gameObject;
			} else
				pick.currObject = null;
		}
		return pick.currObject;
	}
}
