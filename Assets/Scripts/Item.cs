﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	
	public enum interactibles
	{	
		none,
		pickable,
		interact,
		detachable,
		Electric_lock
	};
	public enum quests
	{
		none,
		electric_lock
	}
	public interactibles interactible;
	public quests quest;

	void OnDrawGizmos(){
		Gizmos.color = new Color(1f,0f,0f,1f);
		Gizmos.DrawWireCube (transform.position + this.GetComponent<BoxCollider>().center,this.GetComponent<BoxCollider>().size);
		Gizmos.color = new Color(1f,0f,0f,0.3f);
		Gizmos.DrawCube (transform.position + this.GetComponent<BoxCollider>().center,this.GetComponent<BoxCollider>().size);

	}
	
}
