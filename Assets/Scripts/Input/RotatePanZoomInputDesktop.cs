﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
//using System.Xml.Linq;

public class RotatePanZoomInputDesktop : MonoBehaviour
{
	[Range(0, 100)] 
	public float 
		rotationSpeed = 30;
	[Range (1, 100)]
	public float
		zoomSpeed = 20;
	[Range (1, 100)]
	public float
		PanSpeed = 5;

	public float minVAngle = -40;
	public float maxVAngle = 40;
	public float screenW;
	private Vector3 dragOrigin;
	private float _lastMouseXPos;
	private float _lastMouseYPos;
	//Test Variables
	public float DragSpeed = 0.2f;
	public bool CameraDragging = true;
	public float OuterLeft = -20;
	public float OuterRight = 20;
	public float OuterTop = 400;
	public float OuterBot = -400;
	//Test Variables
	void Start ()
	{
//		Camera.main.transform.LookAt (gameObject.transform);
	}

	void Update ()
	{
		if (!EventSystem.current.IsPointerOverGameObject ()) {
			// Rotation
			if (IsRotationState ()) {
				HandleRotation ();
			}
			
			// Zoom
			HandleZoom ();
			
			// TODO Pan
			HandlePan ();
		}

      
         
	}

	#region rotation

	private void HandleRotation ()
	{
		float deltaX = 0;
		float deltaY = 0;
        
		if (Input.GetMouseButton (0)) {
			float currMouseX = Input.mousePosition.x;
			deltaX = _lastMouseXPos - currMouseX;
			_lastMouseXPos = currMouseX;

			float currMouseY = Input.mousePosition.y;
			deltaY = _lastMouseYPos - currMouseY;
			_lastMouseYPos = currMouseY;
		}

		if (!Input.GetMouseButtonDown (0)) {
			transform.Rotate (Vector3.up * Time.deltaTime * rotationSpeed * deltaX, Space.Self);
			transform.Rotate (Vector3.right * Time.deltaTime * -rotationSpeed * deltaY, Space.World);
			transform.localEulerAngles = new Vector3 (ClampAngle (transform.eulerAngles.x, minVAngle, maxVAngle), transform.eulerAngles.y, 0);
		}
	}

	#endregion

	#region zoom

	private void HandleZoom ()
	{
		float scrollDelta = Input.GetAxis ("Mouse ScrollWheel");
		//       Debug.Log("Scroll " + scrollDelta);
		float newFov = Mathf.Clamp (Camera.main.fieldOfView + scrollDelta * rotationSpeed, 20f, 64.7f);
		Camera.main.fieldOfView = newFov;
	}

	#endregion

	#region Pan

	private void HandlePan ()
	{
//		Vector2 mousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
//        if (mousePosition.x < left)
//        {
//            CameraDragging = true;
//        } else if (mousePosition.x > right)
//        {
//            CameraDragging = true;
//        }   
//        if (mousePosition.y > top)
//        {
//            CameraDragging = true;
//        } else if (mousePosition.y < bot)
//        {
//            CameraDragging = true;
//        }


		if (CameraDragging) {
            
			if (Input.GetMouseButtonDown (1)) {
				dragOrigin = Input.mousePosition;
				return;

			}
            
			if (!Input.GetMouseButton (1))
				return;
          
			Vector3 pos = Camera.main.ScreenToViewportPoint (Input.mousePosition - dragOrigin);
            
			Vector3 moveX = new Vector3 (Mathf.Clamp (pos.x, OuterLeft, OuterRight), 0, 0);
			Vector3 moveY = new Vector3 (0, pos.y, 0);
			transform.Translate (moveX * DragSpeed, Space.World);
			transform.Translate (moveY * DragSpeed, Space.World);
			screenW = Screen.currentResolution.width / 2;

			//transform.position = new Vector3(Mathf.Clamp(transform.position.x,-screenW,screenW),Mathf.Clamp(transform.position.y,transform.position.y - screenW,transform.position.y + screenW),transform.position.z);
			float min = 490.1f;
			float max = 492.3f;
			transform.position = new Vector3 (
				Mathf.Clamp (transform.position.x, -2, 2),
				Mathf.Clamp (transform.position.y, min, max),
				transform.position.z);
//            if (moveX.x > 0f)
//            {
//                if (this.transform.position.x < OuterRight)
//                {
//                    transform.Translate(moveX, Space.World);
//                }
//            } else
//            {
//                if (this.transform.position.x > OuterLeft)      
//                {
//                    transform.Translate(moveX, Space.World);
//                }
//            }
//          
//            if (moveY.y > 0f)
//            {
//                if (this.transform.position.y < OuterTop)
//                {
//                    transform.Translate(moveY, Space.World);
//                }
//
//            } else
//            {
//                if (this.transform.position.y > OuterBot)
//                {
//                    transform.Translate(moveY, Space.World);
//                }
//            }
//
            
		}
    

	}

	#endregion

	private bool IsRotationState ()
	{
		return GetCurrentInputType () == InputType.Rotate;
	}

	private InputType GetCurrentInputType ()
	{
		if (Input.GetMouseButton (0)) {
			return InputType.Rotate;
		}

		return InputType.None;
	}




	private float ClampAngle (float angle, float min, float max)
	{
		angle = Mathf.Repeat (angle, 360);
		min = Mathf.Repeat (min, 360);
		max = Mathf.Repeat (max, 360);
		bool inverse = false;
		var tmin = min;
		var tangle = angle;
		if (min > 180) {
			inverse = !inverse;
			tmin -= 180;
		}
		if (angle > 180) {
			inverse = !inverse;
			tangle -= 180;
		}
		var result = !inverse ? tangle > tmin : tangle < tmin;
		if (!result)
			angle = min;
		
		inverse = false;
		tangle = angle;
		var tmax = max;
		if (angle > 180) {
			inverse = !inverse;
			tangle -= 180;
		}
		if (max > 180) {
			inverse = !inverse;
			tmax -= 180;
		}
		
		result = !inverse ? tangle < tmax : tangle > tmax;
		if (!result)
			angle = max;
		return angle;
	}



}
