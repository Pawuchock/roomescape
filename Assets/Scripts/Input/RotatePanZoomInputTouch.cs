﻿using UnityEngine;


public class RotatePanZoomInputTouch : MonoBehaviour
{
	#region rotationSpeed
	#if UNITY_IPHONE
	[Range(1, 100)] public float rotationSpeed = 10;
	#elif UNITY_ANDROID
	[Range(1, 100)] public float rotationSpeed = 30;
	#elif UNITY_WEBGL
	[Range(1, 100)] public float rotationSpeed = 30;
	#elif UNITY_STANDALONE
	[Range(1, 100)] public float rotationSpeed = 30;

	#elif UNITY_EDITOR
	[Range(1, 100)] public float rotationSpeed = 30;
	#endif
	#endregion
	[Range(1, 100)] public float zoomSpeed = 250;
	[Range(1, 100)] public float panSpeed = 0.2f;
	
	private bool blockZoom;
//	private bool blockPan;
	private const float MaxScale = 2.5f;
	private const float MinScale = 0.3f;
	private const float ScaleFactor = 0.002f;
	
	private const float ZoomThreshold = 0.02f;
	
	void Update()
	{
		HandleRotation();
		HandlePanOrZoom(); 
	}
	
	#region rotation
	
	private void HandleRotation()
	{
		if (Input.touchCount != 1) { return; }
		var touch = Input.GetTouch(0);        
		if (Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			transform.Rotate(Vector3.up * Time.deltaTime * -rotationSpeed * touch.deltaPosition.x, Space.Self);
			transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed * touch.deltaPosition.y, Space.World);
			transform.position = new Vector3(Mathf.Clamp(transform.position.x,-20,20),transform.position.y,transform.position.z);
		}
	}
	#endregion
	
	#region PanOrZoomHandler
	private void HandlePanOrZoom()
	{
		if (Input.touchCount != 2) { return; }
		HandleZoom ();
		HandlePan ();	
		
	}
	
	#endregion
	
	#region zoom
	private void HandleZoom()
	{	
		Touch touch1 = Input.GetTouch(0);
		Touch touch2 = Input.GetTouch(1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
		if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
		{
			Vector2 curDist = touch1.position - touch2.position;
			Vector2 prevDist = (touch1.position - touch1.deltaPosition) - (touch2.position - touch2.deltaPosition);
			float delta = (curDist.magnitude - prevDist.magnitude) * ScaleFactor;
			if (Mathf.Abs(delta) < ZoomThreshold) { return; }
//			
//			float newFov = Mathf.Clamp(Camera.main.fieldOfView - delta * rotationSpeed, 20f, 94.7f);
//			Camera.main.fieldOfView = newFov;

			transform.localScale += new Vector3(delta, delta, delta);
			var clampedScale = Mathf.Clamp(transform.localScale.x, MinScale, MaxScale);
			transform.localScale = new Vector3(clampedScale, clampedScale, clampedScale);
		}
//		blockPan = false;
	}
	#endregion
	
	#region Pan 
	private void HandlePan()
	{	
		
		Touch touch1 = Input.GetTouch(0);
		Touch touch2 = Input.GetTouch(1);  
		if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
		{
			float x = Input.touches[0].deltaPosition.x  * Time.deltaTime;
			float y = Input.touches[0].deltaPosition.y  * Time.deltaTime;
	transform.Translate(Mathf.Clamp(x,-1,1)  * panSpeed, y * panSpeed, 0, Space.World);
		}
		
	}
	#endregion


}
