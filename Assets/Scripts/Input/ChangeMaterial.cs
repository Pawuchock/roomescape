﻿using UnityEngine;
using System.Collections;

public class ChangeMaterial : MonoBehaviour {

	public	Material HighlightedMat;
	public static GameObject obj;
	public GameObject SoftPanel,WoodPanell;
	private GameObject previousObj=null;
	private Material previousMaterial=null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
			ChangePartMaterial ();
	}
	#region MaterialChooser
	
	private void ChangePartMaterial()
	{

		if (Input.touchCount != 1 ) { return; }
		var touch = Input.GetTouch(0);   
		if (touch.phase == TouchPhase.Ended&&touch.tapCount==1) 
		{
				Ray ray = Camera.main.ScreenPointToRay (touch.position);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, 3000)) 
				{
					SetPrevMaterialHit(hit.collider.gameObject);
					obj = hit.collider.gameObject;
					if(previousObj==null||previousObj!=obj)
					{
						previousObj=obj;
						previousMaterial=GetObjectMaterial(obj);
					}
					SetObjectMaterial(ref obj,HighlightedMat);
					var objTag = obj.tag; 
					if (objTag == "Soft") 
					{
						WoodPanell.SetActive (false);
						SoftPanel.SetActive (true);
					} 
					else 
					{
						SoftPanel.SetActive (false);
						WoodPanell.SetActive (true);
					}
				}
				else
				{
//				if (UICamera.hoveredObject.tag=="UIRootPanel") 
//				{
//					Close();
//				}
				}		 
		}
	}

	/// <summary>
	/// Sets previous material and closes wood and soft panels
	/// </summary>
	public void Close()
	{
		SetPrevMaterial ();
		SoftPanel.SetActive (false);
		WoodPanell.SetActive (false);
	}

	/// <summary>
	/// Sets material to an object.
	/// </summary>
	/// <param name="ob">Object that changed.</param>
	/// <param name="mat">Material to set.</param>
	private void SetObjectMaterial(ref GameObject ob,Material mat)
	{
		if (GetObjectMaterial(ob).mainTexture != HighlightedMat.mainTexture) {
			previousMaterial = GetObjectMaterial (ob);
		}
		if (ob.transform.childCount != 0) 
		{
			foreach (Transform child in ob.transform) 
			{
				child.gameObject.GetComponent<Renderer>().material = mat;
			}
		} 
		else 
		{
			ob.GetComponent<Renderer>().material = mat;
		}

	}
	/// <summary>
	/// Gets the object's material.
	/// </summary>
	/// <returns>The object's material.</returns>
	/// <param name="ob">Object.</param>
	private Material GetObjectMaterial(GameObject ob)
	{
		if(ob.transform.childCount!=0)
		{
			return new Material(ob.transform.GetChild(0).gameObject.GetComponent<Renderer>().material);
		}
		else
		{
			return new Material(ob.GetComponent<Renderer>().material);
		}
	}

	/// <summary>
	/// Sets the previous material to object if other object was hitted;
	/// </summary>
	/// <param name="chosenObj">Object that was hitted.</param>
	private void SetPrevMaterialHit(GameObject chosenObj)
	{
		if(previousObj != null)
		{
			if (previousObj.transform.childCount != 0) 
			{
				if (previousObj.transform.GetChild (0).gameObject.GetComponent<Renderer>().material.mainTexture == HighlightedMat.mainTexture && chosenObj != previousObj) 
				{
					SetObjectMaterial (ref previousObj,previousMaterial);
				}
			} 
			else 
			{
				if (previousObj.GetComponent<Renderer>().material.mainTexture == HighlightedMat.mainTexture && chosenObj != previousObj) 
				{
					SetObjectMaterial ( ref previousObj, previousMaterial);
				}
			}
		}
	}

	/// <summary>
	/// Sets the previous material to an object.
	/// </summary>
	private void SetPrevMaterial()
	{
		if (previousObj != null) {
						if (previousObj.transform.childCount != 0) {
								if (previousObj.transform.GetChild (0).gameObject.GetComponent<Renderer>().material.mainTexture == HighlightedMat.mainTexture) {
										SetObjectMaterial (ref previousObj, previousMaterial);
								}
						} else {
								if (previousObj.GetComponent<Renderer>().material.mainTexture == HighlightedMat.mainTexture) {
										SetObjectMaterial (ref previousObj, previousMaterial);
								}
						}
				}
	}
		#endregion
	}
	