﻿public enum InputType
{
    None,
    Rotate,
    Zoom,
    Pan
}
