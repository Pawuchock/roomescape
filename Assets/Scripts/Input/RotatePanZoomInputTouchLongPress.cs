﻿using UnityEngine;
using System.Collections;
//using System.Xml.Linq;

public class RotatePanZoomInputTouchLongPress : MonoBehaviour
{
	[Range (1, 100)] public float rotationSpeed = 10;
	[Range (1, 100)] public float zoomSpeed = 250;
	[Range (1, 100)] public float panSpeed = 0.2f;

	private bool blockZoom;
	private bool blockPan;
	private const float MaxScale = 2.5f;
	private const float MinScale = 0.3f;
	private const float ScaleFactor = 0.002f;

	private const float ZoomThreshold = 0.02f;
	private float x;
	private float y;

	private float touchTime;

	void Update ()
	{
		gestureDetector ();
	}

	#region gestureDetector

	private void gestureDetector ()
	{
		if (Input.touchCount == 2) {
			HandleZoom ();	
		}
		if (Input.touchCount == 1) {
			TouchPhase phase = Input.GetTouch (0).phase;
			if (phase == TouchPhase.Began) {
				touchTime = Time.time;
			}
			if (phase == TouchPhase.Moved) {
				if (Time.time - touchTime <= 0.5) {
					touchTime = Time.time;
					Debug.Log ("Tap");				
					HandleRotation ();
				} else {
					Debug.Log ("LongTap");
					HandlePan ();
				}
			} 
		}
	}

	#endregion

	#region rotation/pan/zoom

	#region rotation

	private void HandleRotation ()
	{
		if (Input.touchCount != 1) {
			return;
		}
		var touch = Input.GetTouch (0);        
		if (Input.GetTouch (0).phase == TouchPhase.Moved) {
			transform.Rotate (Vector3.up * Time.deltaTime * - rotationSpeed * touch.deltaPosition.x, Space.Self);
			transform.Rotate (Vector3.right * Time.deltaTime * rotationSpeed * touch.deltaPosition.y, Space.World);
			transform.position = new Vector3 (Mathf.Clamp (transform.position.x, -20, 20), transform.position.y, transform.position.z);
		}
	}

	#endregion

	#region zoom

	private void HandleZoom ()
	{	
		Touch touch1 = Input.GetTouch (0);
		Touch touch2 = Input.GetTouch (1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
		if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved) {
			Vector2 curDist = touch1.position - touch2.position;
			Vector2 prevDist = (touch1.position - touch1.deltaPosition) - (touch2.position - touch2.deltaPosition);
			float delta = (curDist.magnitude - prevDist.magnitude) * ScaleFactor;
			if (Mathf.Abs (delta) < ZoomThreshold) {
				return;
			}
			transform.localScale += new Vector3 (delta, delta, delta);
			var clampedScale = Mathf.Clamp (transform.localScale.x, MinScale, MaxScale);
			transform.localScale = new Vector3 (clampedScale, clampedScale, clampedScale);
		}
	}

	#endregion

	#region pan

	private void HandlePan ()
	{	
		if (Input.GetTouch (0).phase == TouchPhase.Moved) {
			float x = Input.GetTouch (0).deltaPosition.x * Time.deltaTime;
			float y = Input.GetTouch (0).deltaPosition.y * Time.deltaTime;
			transform.Translate (Mathf.Clamp (x, -1, 1) * panSpeed, y * panSpeed, 0, Space.World);
		}
	}

	#endregion


	#endregion

	#region clampAngle

	private float ClampAngle (float angle, float min, float max)
	{
		angle = Mathf.Repeat (angle, 360);
		min = Mathf.Repeat (min, 360);
		max = Mathf.Repeat (max, 360);
		bool inverse = false;
		var tmin = min;
		var tangle = angle;
		if (min > 180) {
			inverse = !inverse;
			tmin -= 180;
		}
		if (angle > 180) {
			inverse = !inverse;
			tangle -= 180;
		}
		var result = !inverse ? tangle > tmin : tangle < tmin;
		if (!result)
			angle = min;

		inverse = false;
		tangle = angle;
		var tmax = max;
		if (angle > 180) {
			inverse = !inverse;
			tangle -= 180;
		}
		if (max > 180) {
			inverse = !inverse;
			tmax -= 180;
		}

		result = !inverse ? tangle < tmax : tangle > tmax;
		if (!result)
			angle = max;
		return angle;
	}

	#endregion


}
