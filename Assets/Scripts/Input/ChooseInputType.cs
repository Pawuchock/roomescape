﻿using UnityEngine;
using System.Collections;

public class ChooseInputType : MonoBehaviour
{
	
	void Awake ()
	{
		AddInputScript ();
	}

	void AddInputScript ()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			gameObject.AddComponent<RotatePanZoomInputTouchLongPress> ();
		} else {
			gameObject.AddComponent<RotatePanZoomInputDesktop> ();
		}
	}
}
