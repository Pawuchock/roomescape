﻿using UnityEngine;
using System.Collections;

public class Pick : MonoBehaviour
{
	public Camera camera;
	public GameObject currObject = null;
	public GameObject inventoryManager;
	public Cursor_Holder _Cursor_Holder;
	public Quest_manager _Quest_manager;
	// Update is called once per frame

	void Update ()
	{
		detectCollision ();
		_Cursor_Holder.cursor_manager ();
		pickItem (currObject);
	}

	private void pickItem (GameObject object_to_pick)
	{
		
		if (Input.GetMouseButtonDown (0)) {
			
			if (currObject != null) {
				///<summary>
				///Check if currObject exists
				///</summary>		
				if (currObject.GetComponent<Item> () != null) {
					Item.interactibles interactible = currObject.GetComponent<Item> ().interactible;
					///<summary>
					/// Check if currObject has <Item> component
					///</summary>
					switch (interactible) {
		
					case Item.interactibles.none:
						Debug.Log ("none");
						return;
						break;

					case Item.interactibles.pickable:
						Inventory inventoryScript = inventoryManager.GetComponent<Inventory> ();
						inventoryScript.addToInventory (currObject);
						currObject.SetActive (false);
						break;
					case Item.interactibles.interact:
						_Quest_manager.select_quest (object_to_pick);
						Debug.Log ("interact");
						break;

					case Item.interactibles.detachable:
						currObject.SetActive (false);
						break;					
					}
//					Debug.Log ("You picked a " + object_to_pick.name+" which is "+ currObject.GetComponent<Item>().interactible.ToString());
				} else
					return;
			} else
				return;
		}
	}

	private GameObject detectCollision ()
	{
		int x = Screen.width / 2;
		int y = Screen.height / 2;
		Ray ray = camera.ScreenPointToRay (new Vector3 (x, y));
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) { 
			if (hit.distance < 2f) {
//					 if something hit at range of 2f, send the message
//					Debug.Log ("Name " + hit.collider.gameObject.name + " Distance " + hit.distance / 100);
				currObject = hit.collider.gameObject;
			} else
				currObject = null;
		}
		return currObject;
	}

	//	private void cursor_manager ()
	//	{
	//		if (currObject != null) {
	//			if (currObject.GetComponent<Item> () != null) {
	//				if (currObject.GetComponent<Item> ().interactible == Item.interactibles.interact) {
	//					_Cursor_Holder.setActive_cursor (_Cursor_Holder.interact_cursor);
	//				}
	//				if (currObject.GetComponent<Item> ().interactible == Item.interactibles.pickable) {
	//					_Cursor_Holder.setActive_cursor (_Cursor_Holder.hover_cursor);
	//				}
	//				if (currObject.GetComponent<Item> ().interactible == Item.interactibles.detachable) {
	//					_Cursor_Holder.setActive_cursor (_Cursor_Holder.hover_cursor);
	//				}
	//			} else
	//				_Cursor_Holder.setActive_cursor (_Cursor_Holder.default_cursor);
	//		} else
	//			_Cursor_Holder.setActive_cursor (_Cursor_Holder.default_cursor);
	//	}
}
