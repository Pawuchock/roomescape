﻿using UnityEngine;
using System.Collections;

namespace RoomEscape.Audio
{
	public class Audio_Holder : MonoBehaviour
	{
		public static AudioSource Electric_door_open;
		public static AudioSource test1;
		public static AudioSource test2;
		public AudioSource _Electric_door_open;
		public AudioSource _test1;
		public AudioSource _test2;

		private void Awake()
		{
			_Electric_door_open = Electric_door_open;
		}
	}
}