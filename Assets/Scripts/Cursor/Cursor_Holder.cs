﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cursor_Holder : MonoBehaviour
{

	public GameObject active_cursor;
	public Sprite interact_cursor;
	public Sprite hover_cursor;
	public Sprite default_cursor;
	public Pick _Pick;

	public void setActive_cursor (Sprite img)
	{
		active_cursor.GetComponent<Image> ().overrideSprite = img;
	}

	public void cursor_manager ()
	{
		if (_Pick.currObject != null) {
			if (_Pick.currObject.GetComponent<Item> () != null) {
				if (_Pick.currObject.GetComponent<Item> ().interactible == Item.interactibles.interact) {
					setActive_cursor (interact_cursor);
				}
				if (_Pick.currObject.GetComponent<Item> ().interactible == Item.interactibles.pickable) {
					setActive_cursor (hover_cursor);
				}
				if (_Pick.currObject.GetComponent<Item> ().interactible == Item.interactibles.detachable) {
					setActive_cursor (hover_cursor);
				}
			} else
				setActive_cursor (default_cursor);
		} else
			setActive_cursor (default_cursor);
	}
}
