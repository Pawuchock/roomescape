﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Quest_itemSOList : ScriptableObject {
	public List<Quest_itemSO> quest_list = new List<Quest_itemSO>();
}
