﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableObject {
	[MenuItem("Assets/Create/Create Scriptable Item")]

	public static void CreateSO(){
		ItemSO asset = ScriptableObject.CreateInstance<ItemSO>();

		AssetDatabase.CreateAsset(asset,"Assets/Scripts/ScriptableObject/NewScriptableObject.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
	}
}
