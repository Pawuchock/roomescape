﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Quest_itemSO {
	public string quest_name = "Quest Name";
	public QuestItem[] quest_item;
}
[System.Serializable]
public struct QuestItem{
	public GameObject quest_item;
	public Sprite quest_item_image;
}