﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableQuest {
	[MenuItem("Assets/Create/Create Scriptable Quest")]

	public static Quest_itemSOList createQuest(){
		Quest_itemSOList asset = ScriptableObject.CreateInstance<Quest_itemSOList>();

		AssetDatabase.CreateAsset(asset,"Assets/Scripts/ScriptableObject/QuestList.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}
}
