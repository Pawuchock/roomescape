﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ItemSO : ScriptableObject {
	public List<float> floatlist = new List<float>();
}
