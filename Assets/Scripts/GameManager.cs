﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	[Header("Wall Animated")]
	public GameObject animated_wall;
	public GameObject nail;
	[Header("Electric Box Door")]
	public GameObject animated_electric_box_door;
	public GameObject animated_electric_box_door_key;


	// Update is called once per frame
	void FixedUpdate () {
		open_wall_animation ();
		open_animated_electric_box_door ();
	}
	private void open_wall_animation(){
		if(!nail.activeSelf){
			animated_wall.GetComponent<Animator> ().enabled = true;
		}
	}
	private void open_animated_electric_box_door(){
		if(animated_electric_box_door_key.activeSelf){
			animated_electric_box_door.GetComponent<Animator> ().enabled = true;
		
		}
	}
}
