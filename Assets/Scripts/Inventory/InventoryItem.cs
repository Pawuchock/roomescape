﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct InventoryItem {
	public GameObject item_icon;
	public GameObject item_mask;
}
