﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
	public List<GameObject> inventorySpace;

	public void addToInventory (GameObject obj)
	{
		AddToInventory (obj);
	}

	public void removeFromInventory (GameObject obj)
	{
		RemoveFromInventory (obj);
	}


	private void AddToInventory (GameObject obj)
	{
		inventorySpace.Add (obj);	
	}

	private void RemoveFromInventory (GameObject obj)
	{
		inventorySpace.Remove (obj);
	}
}
