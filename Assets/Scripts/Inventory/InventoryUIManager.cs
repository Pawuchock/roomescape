﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryUIManager : MonoBehaviour
{
	Inventory inventoryScript;
	public GameObject item_is_using;
	public InventoryItem[] inventory_item;
	//	public GameObject inventoryManager;

	// Use this for initialization
	void Start ()
	{
		inventoryScript = this.GetComponent<Inventory> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		inventory_filler ();
		use_item ();
	}

	//	private void inventory_filler ()
	//	{
	//
	////		if (inventoryScript.inventorySpace.Count > 0) {
	////			itemIcons [inventoryScript.inventorySpace.Count - 1].SetActive (true);
	////			itemIcons [inventoryScript.inventorySpace.Count - 1].GetComponent<Image> ().sprite = Resources.Load<Sprite> ("inventory_icons/" + inventoryScript.inventorySpace [inventoryScript.inventorySpace.Count - 1].name);
	////		}
	////	
	//
	//	}

	private void use_item ()
	{
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			if (inventoryScript.inventorySpace.Count >= 1) {
				Debug.Log ("you are using" + inventoryScript.inventorySpace [0].name);	
				use_and_mask_item (0);
			} 
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			if (inventoryScript.inventorySpace.Count >= 2) {
				Debug.Log ("you are using" + inventoryScript.inventorySpace [1].name);	 
				use_and_mask_item (1);
			}
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			if (inventoryScript.inventorySpace.Count >= 3) {
				Debug.Log ("you are using" + inventoryScript.inventorySpace [2].name);	 
				use_and_mask_item (2);
			}
		}
		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			if (inventoryScript.inventorySpace.Count >= 4) {
				Debug.Log ("you are using" + inventoryScript.inventorySpace [3].name);	 
				use_and_mask_item (3);
			}
		}
		if (Input.GetKeyDown (KeyCode.Alpha5)) {
			if (inventoryScript.inventorySpace.Count >= 5) {
				Debug.Log ("you are using" + inventoryScript.inventorySpace [4].name);	 
				use_and_mask_item (4);
			}
		}
	}

	private void use_and_mask_item (int i)
	{
		if (item_is_using == inventoryScript.inventorySpace [i]) {
			item_is_using = null;
			inventory_item [i].item_mask.SetActive (false);
		} else {
			item_is_using = inventoryScript.inventorySpace [i];
			set_mask (i);
		}
	}

	private void set_mask (int i)
	{
		foreach (InventoryItem item in inventory_item) {
			item.item_mask.SetActive (false);
		}
		inventory_item [i].item_mask.SetActive (true);
	}

	/// <summary>
	/// !!!!ACHTUNG!!!!BUDLO CODE DETECTED!!!!
	/// </summary>
	private void inventory_filler ()
	{
		if (inventoryScript.inventorySpace.Count >= 1) {
			AddIcon (0);
		} else {
			RemoveIcon (0);
		}

		if (inventoryScript.inventorySpace.Count >= 2) {
			AddIcon (1);
		} else {
			RemoveIcon (1);
		}

		if (inventoryScript.inventorySpace.Count >= 3) {
			AddIcon (2);
		} else {
			RemoveIcon (2);
		}

		if (inventoryScript.inventorySpace.Count >= 4) {
			AddIcon (3);
		} else {
			RemoveIcon (3);
		}

		if (inventoryScript.inventorySpace.Count >= 5) {
			AddIcon (4);
		} else {
			RemoveIcon (4);
		}
	}

	private void AddIcon (int i)
	{
		inventory_item [i].item_icon.SetActive (true);
		inventory_item [i].item_icon.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("inventory_icons/" + inventoryScript.inventorySpace [i].name);
	}

	private void RemoveIcon (int i)
	{
		inventory_item [i].item_icon.SetActive (false);
		inventory_item [i].item_mask.SetActive (false);

	}
}