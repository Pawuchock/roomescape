﻿using UnityEngine;
using System.Collections;
using RoomEscape.Audio;

public class Electric_box_quest : MonoBehaviour
{
	public GameObject electric_box_key;
	public GameObject box_key;
	public Inventory _Inventory;
	public InventoryUIManager _InventoryUIManager;

	public void start_electric_box_quest ()
	{
		if (_InventoryUIManager.item_is_using == electric_box_key) {
			box_key.SetActive (true);
			_Inventory.removeFromInventory (electric_box_key);
			_InventoryUIManager.item_is_using = null;
		} else
			Debug.Log ("not openable");
	}
}
