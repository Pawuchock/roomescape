﻿using UnityEngine;
using System.Collections;

public class Quest_manager : MonoBehaviour
{
	public Electric_box_quest _Electric_box_quest;

	public void select_quest (GameObject quest_object)
	{
		//Debug.Log (quest_object.GetComponent<Item>().quest.ToString ());
		Item.quests quest = quest_object.GetComponent<Item> ().quest;
		switch (quest) {
		case Item.quests.electric_lock:
			_Electric_box_quest.start_electric_box_quest ();
			break;
		}
	}
}
