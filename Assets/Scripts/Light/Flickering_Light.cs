﻿using UnityEngine;
using System.Collections;

public class Flickering_Light : MonoBehaviour {
		public Light flickeringLight;
		public MeshRenderer embientMat;
		public Material turned_on;
		public Material turned_off;

		void Update ()
		{
			float RandomNumber = Random.value;
			if (RandomNumber <= .7) {			
				enableLight ();
			} else
				disableLight ();
		}

		private void enableLight ()
		{
			flickeringLight.enabled = true;
			embientMat.material = turned_on;
		}

		private void disableLight ()
		{
			flickeringLight.enabled = false;
			embientMat.material = turned_off;
		}
}
